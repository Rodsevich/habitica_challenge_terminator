import 'package:angular/angular.dart';

import 'package:habitica_challenge_terminator/angular/components/main.template.dart'
    as ng;

void main() {
  runApp(ng.HabiticaMainComponentNgFactory);
}
