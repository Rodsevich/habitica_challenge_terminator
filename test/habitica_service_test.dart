// @TestOn('browser')
// import 'package:angular_test/angular_test.dart';
// import 'package:habitica_challenge_terminator/angular/components/main.template.dart' as ng;
// import 'package:habitica_challenge_terminator/angular/components/main.dart' show HabiticaMainComponent;
import 'package:habitica_challenge_terminator/angular/habitica_service.dart';
import 'package:habitica_challenge_terminator/bin/personas.dart';
import 'package:habitica_challenge_terminator/model/desafio.dart';
import 'package:habitica_challenge_terminator/model/members.dart';
import 'package:habitica_challenge_terminator/model/persona.dart';
import 'package:habitica_challenge_terminator/model/user.dart';
import 'package:test/test.dart';
import 'dart:math';

void main() {
  // ignore: omit_local_variable_types
  Persona usuario;
  HabiticaService service;
  setUpAll(() {
    usuario = personas['nico'];
    service = HabiticaService();
  });
  group('Desafios:', () {
    var jimena = personas['jimena'];
    var luciana = personas['luciana'];
    var krysia = personas['krysia'];
    for (int i = 0; i < 3; i++) {
      Desafio desafio, nuevoDesafio;
      test('los crea', () async {
        usuario = await service.completarUsuario(usuario);
        desafio = Desafio(
            name: 'prueba 2$i',
            shortName: 'nombre-corto',
            summary: 'sumario',
            prize: 0);
        nuevoDesafio = await service.crearDesafio(usuario, desafio);
        expect(nuevoDesafio, isNotNull);
        expect(nuevoDesafio.name, equals(desafio.name));
        expect(nuevoDesafio.shortName, equals(desafio.shortName));
        expect(nuevoDesafio.prize, equals(desafio.prize));
        expect(nuevoDesafio.summary, equals(desafio.summary));
      });
      test('se unen', () async {
        expect(service.unirseAlDesafio(usuario, desafio), throwsA(anything));
        await Future.wait([
          service.unirseAlDesafio(usuario, nuevoDesafio),
          service.unirseAlDesafio(jimena, nuevoDesafio),
          service.unirseAlDesafio(krysia, nuevoDesafio),
          Future.delayed(Duration(seconds: 3)) //darle tiempo para procesar
        ]);
        var ultimoDesafio =
            await service.unirseAlDesafio(luciana, nuevoDesafio);
        var members = await service.getMembers(usuario, ultimoDesafio).toList();
        expect(members.length, equals(4));
        expect(ultimoDesafio.memberCount, equals(4), skip: true);
        expect(
            nuevoDesafio.memberCount, isNot(equals(ultimoDesafio.memberCount)));
      });
    }
    test('clona', () async {
      var desafios = await service.getDesafiosPropios(usuario);
      var desafioOriginal = desafios.firstWhere((d) => d.name == 'prueba');
      var desafioClonado =
          await service.clonarDesafio(usuario, desafioOriginal);
      expect(desafioOriginal.id, isNot(equals(desafioClonado.id)));
    }, skip: true);
  });
  group('Envios:', () {
    var luciana = personas['luciana'];
    test('gemas', () async {
      var lucianaUserAntes = await service.getUser(luciana);
      var nicoUserAntes = await service.getUser(usuario);
      var cantidadATransferir = Random().nextInt(min(5, nicoUserAntes.gemas));
      var mensaje = Random().nextDouble().toString();
      expect(lucianaUserAntes.inbox.messages.mensajes,
          isNot(anyElement((Mensaje m) => m.text.contains(mensaje))));
      expect(nicoUserAntes.inbox.messages.mensajes,
          isNot(anyElement((Mensaje m) => m.text.contains(mensaje))));
      await service.enviarGemas(
          usuario, Member(id: luciana.id), mensaje, cantidadATransferir);
      var lucianaUserDespues = await service.getUser(luciana);
      var nicoUserDespues = await service.getUser(usuario);
      expect(lucianaUserDespues.gemas,
          equals(lucianaUserAntes.gemas + cantidadATransferir));
      expect(nicoUserDespues.gemas,
          equals(nicoUserAntes.gemas - cantidadATransferir));
      // expect(lucianaUserAntes.inbox.messages.mensajes,
      //     isNot(anyElement(contains(mensaje))));
      // expect(nicoUserAntes.inbox.messages.mensajes,
      //     isNot(anyElement(contains(mensaje))));
      expect(lucianaUserDespues.inbox.messages.mensajes.map((m) => m.text),
          anyElement(contains(mensaje)));
      expect(nicoUserDespues.inbox.messages.mensajes.map((m) => m.text),
          anyElement(contains(mensaje)));
    });
  }, skip: true);
  group('Carga bien los modelos de los ', () {
    List<Desafio> desafios;
    List<Member> members;
    // final testBed = NgTestBed.forComponent<HabiticaMainComponent>(
    //     ng.HabiticaMainComponentNgFactory);
    // NgTestFixture<HabiticaMainComponent> fixture;

    // setUp(() async {
    //   fixture = await testBed.create();
    // });

    // tearDown(disposeAnyRunningTest);
    test('desafios', () async {
      desafios = await service.getDesafiosPropios(usuario);
      expect(desafios.any((d) => d.name.contains('Daily Communion')), isTrue);
      expect(desafios.length < 22, isTrue);
    });
    test('members', () async {
      var desafio = desafios.firstWhere((d) => d.memberCount > 30,
          orElse: () => desafios.first);
      members = await service.getMembers(usuario, desafio).toList();
      expect(members.length, equals(desafio.memberCount));
    });
  }, skip: true);
}
