# Habitica Challenge Terminator

Terminate your challenges of Habitica easily with this tool:  
[https://rodsevich.gitlab.io/habitica_challenge_terminator/](https://rodsevich.gitlab.io/habitica_challenge_terminator/)
