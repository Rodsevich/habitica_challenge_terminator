import 'package:angular/di.dart';
import 'package:markdown/markdown.dart';

@Pipe('md2html')
class Md2Html extends PipeTransform {
  Element imgResolver(String nombre, [String nom]) {
    var ret = Element.empty('img');
    ret.attributes['class'] = 'habitica-emoji';
    ret.attributes['style'] = 'width: 20px; height: 20px;';
    ret.attributes['src'] =
        'https://s3.amazonaws.com/habitica-assets/cdn/emoji/$nombre.png';
    ret.attributes['alt'] = '$nom';
    return ret;
  }

  String transform(String markdown) {
    var emojis = RegExp(':([a-z_0-9]+?):');
    markdown = markdown.replaceAllMapped(
        emojis,
        (m) =>
            '![${m.group(1)}](https://s3.amazonaws.com/habitica-assets/cdn/emoji/${m.group(1)}.png)');
    var ret = markdown == null
        ? null
        : markdownToHtml(markdown, imageLinkResolver: imgResolver);
    return ret;
  }
}
