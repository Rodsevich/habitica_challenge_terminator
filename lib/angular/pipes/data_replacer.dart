import 'package:angular/di.dart';

@Pipe('dataReplacer')
class DataReplacer extends PipeTransform {
  String transform(String input, Map<String, String> data) {
    // print('reemplazando "$input" con $data');
    data?.forEach((k, v) {
      input = input.replaceAll('{{$k}}', v);
    });
    return input;
  }
}
