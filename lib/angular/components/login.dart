import 'dart:html';

import 'package:angular/angular.dart';
import 'package:angular_forms/angular_forms.dart';
import 'package:habitica_challenge_terminator/model/persona.dart';
import 'package:habitica_challenge_terminator/angular/habitica_service.dart';

@Component(
  selector: 'login',
  templateUrl: 'login.html',
  directives: [coreDirectives, formDirectives, ClassProvider(HabiticaService)],
)
class LoginComponent {
  final HabiticaService _habiticaService;

  @Input()
  Persona usuario;

  LoginComponent(this._habiticaService);

  Future<void> iniciarSesion() {
    return _habiticaService.completarUsuario(usuario).then((nuevoUsuario) {
      usuario = nuevoUsuario;
    }).catchError((_) {
      window.alert('Wrong credentials');
    });
  }
}
