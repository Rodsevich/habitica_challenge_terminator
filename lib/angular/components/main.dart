import 'package:angular/angular.dart';
import 'package:habitica_challenge_terminator/model/persona.dart';

import '../habitica_service.dart';
import './desafios.dart';
import './login.dart';

@Component(
  selector: 'habitica',
  templateUrl: 'main.html',
  directives: [coreDirectives, DesafiosComponent, LoginComponent],
  providers: [ClassProvider(HabiticaService)],
)
class HabiticaMainComponent implements OnInit {
  Persona usuario = Persona();
  // final HabiticaService _habiticaService;

  HabiticaMainComponent();
  // HabiticaMainComponent(this._habiticaService);

  @override
  void ngOnInit() {
    usuario = Persona();
  }
}
