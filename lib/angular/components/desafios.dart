import 'dart:html';
import 'dart:math';

import 'package:angular/angular.dart';
import 'package:angular_components/angular_components.dart';
import 'package:angular_components/laminate/overlay/zindexer.dart';
import 'package:angular_components/model/menu/menu.dart';
import 'package:angular_forms/angular_forms.dart';
import 'package:habitica_challenge_terminator/angular/directives/html_inseguro.dart';
import 'package:habitica_challenge_terminator/angular/pipes/data_replacer.dart';
import 'package:habitica_challenge_terminator/angular/pipes/markdown.dart';
import 'package:habitica_challenge_terminator/model/desafio.dart' hide Profile;
import 'package:habitica_challenge_terminator/model/guild.dart';
import 'package:habitica_challenge_terminator/model/members.dart';
import 'package:habitica_challenge_terminator/model/persona.dart';
import 'package:habitica_challenge_terminator/angular/habitica_service.dart';
import '../pipes/data_replacer.dart';

@Component(
  selector: 'desafios',
  templateUrl: 'desafios.html',
  styleUrls: ['desafios.css'],
  directives: [
    ClassProvider(HabiticaService),
    HtmlInseguro,
    DropdownMenuComponent,
    MaterialButtonComponent,
    MaterialIconComponent,
    MaterialCheckboxComponent,
    MaterialMenuComponent,
    MaterialTooltipDirective,
    MaterialProgressComponent,
    MaterialListComponent,
    MaterialSelectItemComponent,
    MaterialDropdownSelectComponent,
    AutoDismissDirective,
    AutoFocusDirective,
    MaterialDialogComponent,
    ModalComponent,
    EscapeCancelsDirective,
    MaterialYesNoButtonsComponent,
    materialInputDirectives,
    formDirectives,
    coreDirectives
  ],
  pipes: [Md2Html, DataReplacer],
  providers: [popupBindings, ClassProvider(ZIndexer), materialProviders],
)
class DesafiosComponent implements OnInit, DoCheck {
  @Input()
  Persona usuario;
  final HabiticaService _habiticaService;
  List<Desafio> desafios = [];
  List<Desafio> _losDesafios = [];
  Desafio desafioDummy = Desafio(name: 'No challenge chosen', memberCount: 0);
  Desafio desafioElegido;
  List<Guild> guilds = [];
  List<Member> miembros = [];
  SelectionModel miembrosSelection = SelectionModel.multi();
  SelectionModel<Guild> guildsSelection = SelectionModel<Guild>.multi();
  MenuModel<MenuItem> desafiosModel = MenuModel.flat([]);
  SelectionModel challengesSelectionModel = SelectionModel<String>.single();
  bool mostrandoDialogoConfirmacion = false;
  int cantGanadores = 1;
  int gemasSecundarias = 1;
  int requestsTerminarMandados = 0,
      requestsTerminarTerminados = 0,
      requestsTerminarMaximos = 0;
  Member ganador = Member(profile: Profile());
  @Input()
  String nuevoNombreDelClonado = '';
  int nuevaCantidadGemasClonado = 0;
  String mensajeAEnviar = '';
  Map<String, String> reemplazos = {};
  String sumario = "";
  String sumarioCustomizado =
      '{{memberCount}} habiticans participated in {{oldName}} challenge.  \n'
      'The winner was {{winners}}. The list of the participants is: {{participants}}';
  bool agregarSumario = false;
  bool clonar = false;
  bool unirse = true;
  bool notificar = false;
  int _previousFiltersAmount = 0;
  bool cargando = true;

  /// Label for the button for multi selection of guilds
  String get filtrosDeGuilds {
    var selectedValues = guildsSelection.selectedValues;
    if (selectedValues.isEmpty) {
      return "Select filtering guilds";
    } else if (selectedValues.length == 1) {
      return selectedValues.single.name;
    } else {
      return "${selectedValues.first.name} + ${selectedValues.length - 1} more";
    }
  }

  @override
  void ngDoCheck() {
    if (mostrandoDialogoConfirmacion) {
      reemplazos['newName'] =
          clonar ? nuevoNombreDelClonado : 'NO-CLONE-NO-NEWNAME-ERROR';
      // hay q cambiar la referencia del mapa xq sino no dispara la
      // actualizacion de la plantilla en el pipe:
      reemplazos = Map.from(reemplazos);
    } else if (guildsSelection.selectedValues.length !=
        _previousFiltersAmount) {
      _previousFiltersAmount = guildsSelection.selectedValues.length;
      actualizarDesafiosModel();
    }
  }

  DesafiosComponent(this._habiticaService);

  void empezarTerminar() {
    nuevoNombreDelClonado = desafioElegido.name;
    nuevaCantidadGemasClonado = desafioElegido.prize;
    mensajeAEnviar =
        'The {{oldName}} challenge ended.  \nCheck [this link]({{link}}) for {{newName}}';
    reemplazos['oldName'] = desafioElegido.name;
    reemplazos['memberCount'] = desafioElegido.memberCount.toString();
    reemplazos['participants'] = miembros
        .map((m) => "${m.profile.name} (@${m.auth.local.username})")
        .join(',');
    reemplazos['winners'] = miembrosSelection.selectedValues
        .map((i) => miembros[i])
        .map((m) => "${m.profile.name} (@${m.auth.local.username})")
        .join(',');
    reemplazos['newName'] = nuevoNombreDelClonado;
    reemplazos['link'] =
        'https://habitica.com/challenges/the-cloned-challenge-UUID';
    mostrandoDialogoConfirmacion = true;
  }

  Future<void> onElegido(Desafio eleccion) async {
    miembrosSelection.clear();
    miembros.clear();
    ganador = null;
    challengesSelectionModel.select(eleccion.name);
    desafioElegido = eleccion;
    await for (Member miembro
        in _habiticaService.getMembers(usuario, desafioElegido)) {
      miembros.add(miembro);
    }
  }

  void marcarGanadores(_) {
    miembrosSelection.clear();
    ganador = null;
    var r = Random();
    for (var i = 0; i < cantGanadores; i++) {
      int n;
      do {
        n = r.nextInt(miembros.length);
      } while (miembrosSelection.isSelected(n));
      elegirMiembro(n);
    }
  }

  Future terminarDesafio(_) async {
    if ((cantGanadores - 1) * gemasSecundarias > usuario.gemas) {
      window.alert(
          'You don\'t have enough gems for rewarding $cantGanadores winners');
    }
    mostrandoDialogoConfirmacion = false;
    var acciones = <Future>{};
    Desafio clonado;
    if (clonar) {
      var nuevoDesafio = Desafio.fromJson(desafioElegido.toJson());
      nuevoDesafio.name = nuevoNombreDelClonado;
      nuevoDesafio.prize = nuevaCantidadGemasClonado;
      clonado = await _habiticaService.clonarDesafio(usuario, nuevoDesafio);
      if (unirse) {
        acciones.add(_habiticaService.unirseAlDesafio(usuario, clonado));
      }
    }
    acciones
        .add(_habiticaService.ganarDesafio(usuario, desafioElegido, ganador));
    var mensaje = DataReplacer().transform(mensajeAEnviar, reemplazos);
    if (clonar) {
      mensaje = mensaje.replaceAll('the-cloned-challenge-UUID', clonado.id);
    }
    List<Member> secundarios;
    if (cantGanadores > 1) {
      secundarios = miembrosSelection.selectedValues
          .map((i) => miembros[i])
          .where((m) => m != ganador)
          .toList();
      secundarios.forEach((miembroSecundario) {
        acciones.add(_habiticaService.enviarGemas(
            usuario, miembroSecundario, mensaje, gemasSecundarias));
      });
    }
    if (notificar) {
      miembros
          .where((m) => !(secundarios?.contains(m) ?? false))
          .forEach((miembro) {
        acciones.add(_habiticaService.enviarMensaje(usuario, miembro, mensaje));
      });
    }
    requestsTerminarMaximos = acciones.length;
    requestsTerminarMandados = 0;
    requestsTerminarTerminados = 0;
    await Future.wait(acciones.map((f) async {
      requestsTerminarMandados++;
      await f;
      requestsTerminarTerminados++;
    })).catchError((_) => null);
    window.alert('All ${acciones.length} actions finished.');
    requestsTerminarMaximos = 0;
    requestsTerminarMandados = 0;
    requestsTerminarTerminados = 0;
    desafioElegido = desafioDummy;
    miembrosSelection.clear();
    miembros.clear();
    _habiticaService.completarUsuario(usuario).then((u) {
      usuario = u;
    });
    cargarDesafios();
    if (agregarSumario) {
      var data = DataReplacer().transform(sumarioCustomizado, reemplazos);
      if (clonar) {
        data = data.replaceAll('the-cloned-challenge-UUID', clonado.id);
      } else {
        if (data.contains('the-cloned-challenge-UUID')) {
          window.alert(
              "{{link}} requested for summary, but it doesn't exists as challenge hasn't been cloned.");
          data = DataReplacer()
              .transform(sumarioCustomizado, reemplazos..remove('link'));
        }
      }
      sumario += '$data\n\n---\n';
    }
  }

  void elegirMiembro(int eleccion) {
    if (miembrosSelection.isSelected(eleccion) ||
        miembrosSelection.selectedValues.length == cantGanadores) {
      if (ganador == miembros[eleccion]) {
        ganador = null;
      }
      miembrosSelection.deselect(eleccion);
    } else {
      miembrosSelection.select(eleccion);
      ganador ??= miembros[eleccion];
    }
  }

  @override
  Future<void> ngOnInit() async {
    desafioElegido = desafioDummy;
    await cargarDesafios();
  }

  Future<void> cargarDesafios() async {
    desafios.clear();
    _losDesafios = await _habiticaService.getDesafiosPropios(usuario);
    // desafios.sort((d1, d2) =>
    //     DateTime.parse(d2.createdAt).compareTo(DateTime.parse(d1.createdAt)));
    guilds = await _habiticaService.getGuilds(usuario.user);
    actualizarDesafiosModel();
    cargando = false;
  }

  void actualizarDesafiosModel() {
    desafios = _losDesafios
        .where((d) =>
            guildsSelection.isEmpty ||
            guildsSelection.selectedValues.any((g) => g.id == d.group.id))
        .toList();
    desafios.sort((d1, d2) => d2.memberCount.compareTo(d1.memberCount));
    desafiosModel = MenuModel.flat(
        desafios
            .map<MenuItem<NullMenuItem>>((d) => MenuItem<NullMenuItem>(
                    '(${d.memberCount}) ${d.name}', action: () {
                  onElegido(d);
                }, tooltip: d.description))
            .toList(),
        icon: Icon('menu'),
        tooltipText: 'The challenge to work with');
  }
}
