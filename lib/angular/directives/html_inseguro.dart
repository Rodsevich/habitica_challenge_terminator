import 'dart:html' as dom;
import 'package:angular/angular.dart';

@Directive(selector: '[html-inseguro]')
class HtmlInseguro {
  final dom.Element element;

  final dom.NodeValidatorBuilder _validador;

  HtmlInseguro(this.element)
      : _validador = dom.NodeValidatorBuilder()
          ..allowHtml5()
          ..allowElement('a', attributes: ['href'], uriPolicy: _TodoUriPolicy())
          ..allowElement('img',
              attributes: ['style', 'class', 'alt', 'src'],
              uriAttributes: ['style', 'class', 'alt', 'src'],
              uriPolicy: _TodoUriPolicy());

  @Input('html-inseguro')
  set htmlInseguro(String value) {
    element.setInnerHtml(value?.toString() ?? '', validator: _validador);
  }
}

class _TodoUriPolicy implements dom.UriPolicy {
  @override
  bool allowsUri(String uri) {
    return true;
  }
}
