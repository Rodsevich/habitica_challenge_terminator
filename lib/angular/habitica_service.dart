import 'package:fnx_rest/fnx_rest_browser.dart';
// import 'package:fnx_rest/fnx_rest_io.dart';
import 'package:habitica_challenge_terminator/model/desafio.dart';
import 'package:habitica_challenge_terminator/model/guild.dart';
import 'package:habitica_challenge_terminator/model/members.dart';
import 'package:habitica_challenge_terminator/model/mision.dart';
import 'package:habitica_challenge_terminator/model/persona.dart';
import 'package:habitica_challenge_terminator/model/user.dart';

class HabiticaService {
  RestClient _client;
  RestClient _desafiosClient;
  RestClient _membersClient;
  RestClient _userClient;
  RestClient _guildsClient;

  HabiticaService() {
    _client = BrowserRestClient.root('https://habitica.com/api/v3');
    // _client = IoRestClient.root('https://habitica.com/api/v3');
    _client.setHeader('x-client',
        '841b8071-42ae-403c-9cc8-36acebd81a91-HabiticaChallengeTerminator');
    _desafiosClient = _client.child('/challenges');
    _membersClient = _client.child('/members');
    _userClient = _client.child('/user');
    _guildsClient = _client.child('/groups');
  }

  void _setHeadersFor(Persona usuario) {
    _client.setHeader('x-api-user', usuario.id);
    _client.setHeader('x-api-key', usuario.token);
  }

  Future<List<Guild>> getGuilds(User usuario) async {
    _guildsClient.setParam('type', 'guilds');
    var guildsGET = await _guildsClient.get();
    var groupReq = GuildReq.fromJson(guildsGET.data);
    if (groupReq.success) {
      return groupReq.data;
    } else {
      throw StateError(
          guildsGET.data['error'] + ': ' + guildsGET.data['message']);
    }
  }

  Future<User> getUser(Persona usuario) async {
    _setHeadersFor(usuario);
    var userGET = await _userClient.get();
    var userReq = UserRequest.fromJson(userGET.data);
    if (userReq.success) {
      return userReq.data;
    } else {
      throw ArgumentError('Wrong credentials');
    }
  }

  Future<List<Mision>> getMisiones() async {
    throw UnimplementedError('No gastes timepo en las misiones/quests');
  }

  Future<bool> enviar(Persona usuario, MemberSend envio) async {
    _setHeadersFor(usuario);
    var mensajeClient = _membersClient.child(
        envio.gemAmount != null ? '/transfer-gems' : '/send-private-message');
    RestResult res;
    int tries = 3;
    do {
      res = await mensajeClient.post(envio.toJson());
    } while (tries-- > 0 && res.error);
    return res.success;
  }

  Future<bool> enviarMensaje(Persona usuario, Member miembro, String mensaje) {
    return enviar(usuario, MemberSend(mensaje, miembro));
  }

  Future<bool> enviarGemas(
      Persona usuario, Member miembro, String mensaje, int gemas) {
    return enviar(usuario, MemberSend(mensaje, miembro, gemas));
  }

  Future<List<Desafio>> getDesafios(Persona usuario) async {
    _setHeadersFor(usuario);
    var desafiosGET = await _desafiosClient.child('/user').get();
    desafiosGET.assertSuccess();
    var req = DesafiosRequest.fromJson(desafiosGET.data);
    return req.data;
  }

  Future<List<Desafio>> getDesafiosPropios(Persona usuario) async {
    var desafios = await getDesafios(usuario);
    return desafios.where((d) => d?.leader?.id == usuario.id).toList();
  }

  Future<bool> ganarDesafio(
      Persona usuario, Desafio desafio, Member miembro) async {
    _setHeadersFor(usuario);
    var ganadorClient =
        _desafiosClient.child('/${desafio.id}/selectWinner/${miembro.id}');
    var res = await ganadorClient.post(null);
    res.assertSuccess();
    return res.success;
  }

  Future<Desafio> clonarDesafio(Persona usuario, Desafio desafio) async {
    _setHeadersFor(usuario);
    var clonacionClient = _desafiosClient.child('/${desafio.id}/clone');
    var desafioAClonar = desafio.toJson();
    desafioAClonar['group'] = desafio?.group?.id;
    var res = await clonacionClient.post(desafioAClonar);
    res.assertSuccess();
    return Desafio.fromJson(res.data['data']['clonedChallenge']);
  }

  Future<Desafio> crearDesafio(Persona usuario, Desafio desafio) async {
    _setHeadersFor(usuario);
    var desafioAClonar = desafio.toJson();
    desafioAClonar['group'] = usuario.partyId;
    var res = await _desafiosClient.post(desafioAClonar);
    res.assertSuccess();
    return Desafio.fromJson(res.data['data']);
  }

  Future<Desafio> unirseAlDesafio(Persona usuario, Desafio desafio) async {
    _setHeadersFor(usuario);
    var unirseClient = _desafiosClient.child('/${desafio.id}/join');
    var res = await unirseClient.post(null);
    res.assertSuccess();
    return Desafio.fromJson(res.data['data']);
  }

  Stream<Member> getMembers(Persona usuario, Desafio desafio) async* {
    _setHeadersFor(usuario);
    var membersClient = _desafiosClient.child('/${desafio.id}/members');
    var req = await _makeMembersRequest(membersClient);
    yield* _yieldMembers(req);
    for (var miembros = 30; miembros < desafio.memberCount; miembros += 30) {
      membersClient.setParam('lastId', req.data.last.id);
      req = await _makeMembersRequest(membersClient);
      yield* _yieldMembers(req);
    }
    return;
  }

  Future<MembersRequest> _makeMembersRequest(RestClient membersClient) async {
    var membersGET = await membersClient.get();
    membersGET.assertSuccess();
    return MembersRequest.fromJson(membersGET.data);
  }

  Stream<Member> _yieldMembers(MembersRequest req) async* {
    for (var member in req.data) {
      yield member;
    }
  }

  Future<Persona> completarUsuario(Persona usuario) async {
    var user = await getUser(usuario); //throwea si están mal
    usuario.user = user;
    return usuario;
  }
}
