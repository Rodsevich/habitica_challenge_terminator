import '../model/persona.dart';

Map<String, Persona> personas = {
  'nico': Persona(
      nombre: 'Nico',
      id: '841b8071-42ae-403c-9cc8-36acebd81a91',
      atributo: Atributo.int,
      mail: 'nicordosevich@gmail.com')
};
