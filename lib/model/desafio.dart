class DesafiosRequest {
  bool success;
  List<Desafio> data;
  int userV;
  String appVersion;

  DesafiosRequest({this.success, this.data, this.userV, this.appVersion});

  DesafiosRequest.fromJson(Map<String, dynamic> json) {
    success = json['success'];
    if (json['data'] != null) {
      data = new List<Desafio>();
      json['data'].forEach((v) {
        data.add(new Desafio.fromJson(v));
      });
    }
    userV = json['userV'];
    appVersion = json['appVersion'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this.success;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    data['userV'] = this.userV;
    data['appVersion'] = this.appVersion;
    return data;
  }
}

class Desafio {
  TasksOrder tasksOrder;
  bool official;
  int memberCount;
  int prize;
  String sId;
  String name;
  String summary;
  String description;
  List<Categories> categories;
  Group group;
  Leader leader;
  String shortName;
  String createdAt;
  String updatedAt;
  String id;

  String toString() => name;

  Desafio(
      {this.tasksOrder,
      this.official,
      this.memberCount,
      this.prize,
      this.sId,
      this.name,
      this.summary,
      this.description,
      this.categories,
      this.group,
      this.leader,
      this.shortName,
      this.createdAt,
      this.updatedAt,
      this.id});

  Desafio.fromJson(Map<String, dynamic> json) {
    tasksOrder = json['tasksOrder'] != null
        ? new TasksOrder.fromJson(json['tasksOrder'])
        : null;
    official = json['official'];
    memberCount = json['memberCount'];
    prize = json['prize'];
    sId = json['_id'];
    name = json['name'];
    summary = json['summary'];
    description = json['description'];
    if (json['categories'] != null) {
      categories = new List<Categories>();
      json['categories'].forEach((v) {
        categories.add(new Categories.fromJson(v));
      });
    }
    group = json['group'] != null && json['group'] is! String
        ? Group.fromJson(json['group'])
        : null;
    leader = json['leader'] != null && json['leader'] is! String
        ? Leader.fromJson(json['leader'])
        : null;
    shortName = json['shortName'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.tasksOrder != null) {
      data['tasksOrder'] = this.tasksOrder.toJson();
    }
    data['official'] = this.official;
    data['memberCount'] = this.memberCount;
    data['prize'] = this.prize;
    data['_id'] = this.sId;
    data['name'] = this.name;
    data['summary'] = this.summary;
    data['description'] = this.description;
    if (this.categories != null) {
      data['categories'] = this.categories.map((v) => v.toJson()).toList();
    }
    if (this.group != null) {
      data['group'] = this.group.toJson();
    }
    if (this.leader != null) {
      data['leader'] = this.leader.toJson();
    }
    data['shortName'] = this.shortName;
    data['createdAt'] = this.createdAt;
    data['updatedAt'] = this.updatedAt;
    data['id'] = this.id;
    return data;
  }
}

class TasksOrder {
  List<String> habits;
  List<String> dailys;
  List<String> todos;
  List<String> rewards;

  TasksOrder({this.habits, this.dailys, this.todos, this.rewards});

  TasksOrder.fromJson(Map<String, dynamic> json) {
    habits = json['habits'].cast<String>();
    dailys = json['dailys'].cast<String>();
    todos = json['todos'].cast<String>();
    rewards = json['rewards'].cast<String>();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['habits'] = this.habits;
    data['dailys'] = this.dailys;
    data['todos'] = this.todos;
    data['rewards'] = this.rewards;
    return data;
  }
}

class Categories {
  String sId;
  String slug;
  String name;

  Categories({this.sId, this.slug, this.name});

  Categories.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    slug = json['slug'];
    name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.sId;
    data['slug'] = this.slug;
    data['name'] = this.name;
    return data;
  }
}

class Group {
  String privacy;
  String sId;
  String leader;
  String name;
  String type;
  List<Categories> categories;
  String summary;
  String id;

  Group(
      {this.privacy,
      this.sId,
      this.leader,
      this.name,
      this.type,
      this.categories,
      this.summary,
      this.id});

  Group.fromJson(Map<String, dynamic> json) {
    privacy = json['privacy'];
    sId = json['_id'];
    leader = json['leader'];
    name = json['name'];
    type = json['type'];
    if (json['categories'] != null) {
      categories = new List<Categories>();
      json['categories'].forEach((v) {
        categories.add(new Categories.fromJson(v));
      });
    }
    summary = json['summary'];
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['privacy'] = this.privacy;
    data['_id'] = this.sId;
    data['leader'] = this.leader;
    data['name'] = this.name;
    data['type'] = this.type;
    if (this.categories != null) {
      data['categories'] = this.categories.map((v) => v.toJson()).toList();
    }
    data['summary'] = this.summary;
    data['id'] = this.id;
    return data;
  }
}

class Leader {
  Auth auth;
  Contributor contributor;
  Flags flags;
  Profile profile;
  String sId;
  String id;
  Backer backer;

  Leader(
      {this.auth,
      this.contributor,
      this.flags,
      this.profile,
      this.sId,
      this.id,
      this.backer});

  Leader.fromJson(Map<String, dynamic> json) {
    auth = json['auth'] != null ? new Auth.fromJson(json['auth']) : null;
    contributor = json['contributor'] != null
        ? new Contributor.fromJson(json['contributor'])
        : null;
    flags = json['flags'] != null ? new Flags.fromJson(json['flags']) : null;
    profile =
        json['profile'] != null ? new Profile.fromJson(json['profile']) : null;
    sId = json['_id'];
    id = json['id'];
    backer =
        json['backer'] != null ? new Backer.fromJson(json['backer']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.auth != null) {
      data['auth'] = this.auth.toJson();
    }
    if (this.contributor != null) {
      data['contributor'] = this.contributor.toJson();
    }
    if (this.flags != null) {
      data['flags'] = this.flags.toJson();
    }
    if (this.profile != null) {
      data['profile'] = this.profile.toJson();
    }
    data['_id'] = this.sId;
    data['id'] = this.id;
    if (this.backer != null) {
      data['backer'] = this.backer.toJson();
    }
    return data;
  }
}

class Auth {
  Local local;

  Auth({this.local});

  Auth.fromJson(Map<String, dynamic> json) {
    local = json['local'] != null ? new Local.fromJson(json['local']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.local != null) {
      data['local'] = this.local.toJson();
    }
    return data;
  }
}

class Local {
  String username;

  Local({this.username});

  Local.fromJson(Map<String, dynamic> json) {
    username = json['username'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['username'] = this.username;
    return data;
  }
}

class Contributor {
  String text;
  int level;
  bool admin;
  String contributions;
  bool sudo;

  Contributor(
      {this.text, this.level, this.admin, this.contributions, this.sudo});

  Contributor.fromJson(Map<String, dynamic> json) {
    text = json['text'];
    level = json['level'];
    admin = json['admin'];
    contributions = json['contributions'];
    sudo = json['sudo'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['text'] = this.text;
    data['level'] = this.level;
    data['admin'] = this.admin;
    data['contributions'] = this.contributions;
    data['sudo'] = this.sudo;
    return data;
  }
}

class Flags {
  bool verifiedUsername;

  Flags({this.verifiedUsername});

  Flags.fromJson(Map<String, dynamic> json) {
    verifiedUsername = json['verifiedUsername'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['verifiedUsername'] = this.verifiedUsername;
    return data;
  }
}

class Profile {
  String name;

  Profile({this.name});

  Profile.fromJson(Map<String, dynamic> json) {
    name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    return data;
  }
}

class Backer {
  bool tokensApplied;
  int tier;

  Backer({this.tokensApplied, this.tier});

  Backer.fromJson(Map<String, dynamic> json) {
    tokensApplied = json['tokensApplied'];
    tier = json['tier'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['tokensApplied'] = this.tokensApplied;
    data['tier'] = this.tier;
    return data;
  }
}
