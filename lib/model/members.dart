class MemberSend {
  final String message;
  final Member member;
  final int gemAmount;

  MemberSend(this.message, this.member, [this.gemAmount]);

  Map<String, dynamic> toJson() {
    var ret = <String, dynamic>{};
    ret['message'] = message;
    ret['toUserId'] = member.id;
    if (gemAmount != null) {
      ret['gemAmount'] = gemAmount;
    }
    return ret;
  }
}

class MembersRequest {
  List<Member> data;

  MembersRequest({this.data});

  MembersRequest.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = new List<Member>();
      json['data'].forEach((v) {
        data.add(new Member.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Member {
  String sId;
  Profile profile;
  Flags flags;
  Auth auth;
  String id;

  @override
  String toString() => profile.toString();

  Member({this.sId, this.profile, this.flags, this.auth, this.id});

  Member.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    profile =
        json['profile'] != null ? new Profile.fromJson(json['profile']) : null;
    flags = json['flags'] != null ? new Flags.fromJson(json['flags']) : null;
    auth = json['auth'] != null ? new Auth.fromJson(json['auth']) : null;
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.sId;
    if (this.profile != null) {
      data['profile'] = this.profile.toJson();
    }
    if (this.flags != null) {
      data['flags'] = this.flags.toJson();
    }
    if (this.auth != null) {
      data['auth'] = this.auth.toJson();
    }
    data['id'] = this.id;
    return data;
  }
}

class Profile {
  String name;

  Profile({this.name});

  String toString() => name;

  Profile.fromJson(Map<String, dynamic> json) {
    name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    return data;
  }
}

class Flags {
  bool verifiedUsername;

  Flags({this.verifiedUsername});

  Flags.fromJson(Map<String, dynamic> json) {
    verifiedUsername = json['verifiedUsername'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['verifiedUsername'] = this.verifiedUsername;
    return data;
  }
}

class Auth {
  Local local;

  Auth({this.local});

  Auth.fromJson(Map<String, dynamic> json) {
    local = json['local'] != null ? new Local.fromJson(json['local']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.local != null) {
      data['local'] = this.local.toJson();
    }
    return data;
  }
}

class Local {
  String username;

  Local({this.username});

  Local.fromJson(Map<String, dynamic> json) {
    username = json['username'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['username'] = this.username;
    return data;
  }
}
