class GuildReq {
  bool success;
  List<Guild> data;
  int userV;
  String appVersion;

  GuildReq({this.success, this.data, this.userV, this.appVersion});

  GuildReq.fromJson(Map<String, dynamic> json) {
    success = json['success'];
    if (json['data'] != null) {
      data = new List<Guild>();
      json['data'].forEach((v) {
        data.add(new Guild.fromJson(v));
      });
    }
    userV = json['userV'];
    appVersion = json['appVersion'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this.success;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    data['userV'] = this.userV;
    data['appVersion'] = this.appVersion;
    return data;
  }
}

class Guild {
  String privacy;
  int memberCount;
  double balance;
  String sId;
  String leader;
  String type;
  String name;
  String description;
  List<Categories> categories;
  String summary;
  String id;

  Guild(
      {this.privacy,
      this.memberCount,
      this.balance,
      this.sId,
      this.leader,
      this.type,
      this.name,
      this.description,
      this.categories,
      this.summary,
      this.id});

  Guild.fromJson(Map<String, dynamic> json) {
    privacy = json['privacy'];
    memberCount = json['memberCount'];
    balance = json['balance'];
    sId = json['_id'];
    leader = json['leader'];
    type = json['type'];
    name = json['name'];
    description = json['description'];
    if (json['categories'] != null) {
      categories = new List<Categories>();
      json['categories'].forEach((v) {
        categories.add(new Categories.fromJson(v));
      });
    }
    summary = json['summary'];
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['privacy'] = this.privacy;
    data['memberCount'] = this.memberCount;
    data['balance'] = this.balance;
    data['_id'] = this.sId;
    data['leader'] = this.leader;
    data['type'] = this.type;
    data['name'] = this.name;
    data['description'] = this.description;
    if (this.categories != null) {
      data['categories'] = this.categories.map((v) => v.toJson()).toList();
    }
    data['summary'] = this.summary;
    data['id'] = this.id;
    return data;
  }

  String toString() => name;
}

class Categories {
  String slug;
  String name;
  String sId;

  Categories({this.slug, this.name, this.sId});

  Categories.fromJson(Map<String, dynamic> json) {
    slug = json['slug'];
    name = json['name'];
    sId = json['_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['slug'] = this.slug;
    data['name'] = this.name;
    data['_id'] = this.sId;
    return data;
  }
}
