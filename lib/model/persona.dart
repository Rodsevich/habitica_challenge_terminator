import 'package:habitica_challenge_terminator/model/user.dart';

class Persona {
  String nombre;
  String mail;
  String pass;
  String id;
  String token;
  Atributo atributo;
  User user;

  String get partyId => user.partyId ?? '4e3605ca-9d84-494b-9151-8626e5207a1b';
  bool get confirmado => user != null;
  int get gemas => user.gemas;

  Persona(
      {this.atributo, this.nombre, this.mail, this.pass, this.id, this.token});
}

enum Atributo { str, int, con, per }
