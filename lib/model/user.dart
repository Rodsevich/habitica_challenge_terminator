class UserRequest {
  bool success;
  User data;
  int userV;
  String appVersion;

  UserRequest({this.success, this.data, this.userV, this.appVersion});

  UserRequest.fromJson(Map<String, dynamic> json) {
    success = json['success'];
    data = json['data'] != null ? new User.fromJson(json['data']) : null;
    userV = json['userV'];
    appVersion = json['appVersion'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this.success;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    data['userV'] = this.userV;
    data['appVersion'] = this.appVersion;
    return data;
  }
}

class User {
  Inbox inbox;
  int gemas;
  String id;
  bool needsCron;
  String partyId;

  User({this.inbox, this.gemas, this.id, this.needsCron});

  User.fromJson(Map<String, dynamic> json) {
    inbox = json['inbox'] != null ? new Inbox.fromJson(json['inbox']) : null;
    gemas = (json['balance'] * 4).toInt();
    id = json['id'];
    needsCron = json['needsCron'];
    partyId = json['party']['_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.inbox != null) {
      data['inbox'] = this.inbox.toJson();
    }
    data['balance'] = gemas * .25;
    data['id'] = this.id;
    data['needsCron'] = this.needsCron;
    data['partyId'] = this.partyId;
    return data;
  }
}

class Inbox {
  Messages messages;

  Inbox({this.messages});

  Inbox.fromJson(Map<String, dynamic> json) {
    messages =
        json['messages'] != null ? Messages.fromJson(json['messages']) : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (messages != null) {
      data['messages'] = messages.toJson();
    }
    return data;
  }
}

class Messages {
  List<Mensaje> mensajes;

  Messages({this.mensajes});

  Messages.fromJson(Map<String, dynamic> json) {
    mensajes = [];
    json.values.forEach((v) {
      mensajes.add(Mensaje.fromJson(v));
    });
  }

  Map<String, dynamic> toJson() {
    var data = <String, dynamic>{};
    if (mensajes != null) {
      data['mensajes'] = mensajes.map((m) => m.toJson()).toList();
    }
    return data;
  }
}

class Mensaje {
  String id;
  String text;
  String timestamp;
  String userUUID;
  String user;
  String username;

  @override
  String toString() => '$user(@$username/$userUUID): $text [$timestamp]';

  Mensaje(
      {this.id,
      this.text,
      this.timestamp,
      this.userUUID,
      this.user,
      this.username});

  Mensaje.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    text = json['text'];
    timestamp = json['timestamp'];
    userUUID = json['uuid'];
    user = json['user'];
    username = json['username'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['text'] = text;
    data['timestamp'] = timestamp;
    data['uuid'] = userUUID;
    data['user'] = user;
    data['username'] = username;
    return data;
  }
}
